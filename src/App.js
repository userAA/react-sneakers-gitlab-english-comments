import React from 'react'
import {Route} from "react-router-dom"
import axios from 'axios'

import Header from './components/Header';
import Drawer from './components/Drawer/index';
import AppContext from './context';

import Home from './pages/Home';
import Orders from './pages/Orders';

function App() 
{
  //full list of products
  const [items, setItems] = React.useState([]);
  //the list of products in cars
  const [cartItems, setCartItems] = React.useState([]);
  //minimum price of necessary products
  const [searchValue, setSearchValue] = React.useState('');
  //flag of opening product baskets
  const [cartOpened, setCartOpened] = React.useState(false);
  //data loading flag
  const [isLoading, setIsLoading] = React.useState(true);

  //This hook useEffect at first works every time when the site is restarted 
  //and then at every changing of every from properties located in []
  React.useEffect(() => {
    async function fetchData() {
      try 
      {
        //we download all the data about the products from the server, which you can choose
        //const [itemsResponse] = await Promise.all([
        //  axios.get('https://60d62397943aa60017768e77.mockapi.io/items')
        //]);
        const [itemsResponse] = await Promise.all([
          axios.get('https://react-sneakers-deploy-server.herokuapp.com/items')
        ]);
      
        //loading of data is finished
        setIsLoading(false);

        //All products which can be selected
        setItems((itemsResponse).data);
      } 
      catch (error) 
      { 
        alert('Error when requesting data; (')
        console.error(error);
      }
    }

    fetchData();
  }, [])

  //the function of filling the basket of products (if the product that is already in the basket is added, it is removed from the basket)
  const onAddToCart = async (obj) => {
    try 
    {
      //removing the product from basket
      const findItem = cartItems.find((item) => Number(item.id) === Number(obj.id)); 
      if (findItem) 
      {
        setCartItems((prev) => prev.filter((item) => Number(item.id) !== Number(obj.id)));
      } 
      else 
      {
        //adding new product in to basket
        setCartItems((prev) => [...prev, obj]);
      }
    } 
    catch (error) 
    {
      alert('Error when adding to the cart');
    }
  }

  //the function of removing product from basket
  const onRemoveItem = (id) => 
  {
    try 
    {
      //removing product from basket
      setCartItems((prev) => prev.filter((item) => Number(item.id) !== Number(id)));
    } 
    catch (error) 
    {
      alert('Error when removing from the cart');
      console.error(error);
    }
  }

  //the function of changing minimum price of needed product
  const onChangeSearchInput = (event) => 
  {
    setSearchValue(event.target.value);
  }

  //a function that checks whether an item that is already being added exists in the cart
  const isItemAdded = (id) => 
  {
    return cartItems.some(obj => Number(obj.id) === Number(id))
  };

  return (
    <AppContext.Provider 
      //creating context
      value ={{
        cartItems, 
        isItemAdded,
        setCartOpened,
        setCartItems
      }}>
      <div className="wrapper clear">
        {/*Shopping cart */}
        <Drawer 
          onClose={() => setCartOpened(false)} 
          onRemove={onRemoveItem}  
          items={cartItems}
          opened={cartOpened}
        /> 
        
        {/*The header, we sending the product opening function to it */}
        <Header onClickCart={() => setCartOpened(true)}/>
      
        {/*Central page internet - shop*/}
        <Route path="/" exact>
          <Home
            items={items}
            searchValue={searchValue}
            setSearchValue={setSearchValue}
            onChangeSearchInput={onChangeSearchInput}
            onAddToCart={onAddToCart}
            isLoading={isLoading}
          />
        </Route>

        {/*Transmit on page of designed orders */}
        <Route path="/orders" exact>
          <Orders/>
        </Route>
      </div>
    </AppContext.Provider>
  );
}

export default App; 