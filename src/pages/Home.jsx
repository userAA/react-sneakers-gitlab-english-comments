import React from "react";

import Card from '../components/Card/index';

function Home({
    //full list of products
    items,               
    //the desired minimum price of the product, entered by the user
    searchValue,         
    //retainer of desired minimum price of the product, entering by user
    setSearchValue,      
    //the function entering of minimum price of the product desired by user
    onChangeSearchInput,
    //the function of adding product in to cart
    onAddToCart,        
    //flag for the formation of the entire original product page
    isLoading            
})  {
    
    //the function of containing of list products based on the minimum price
    const renderItems = () => {
        //containing the list of products based on the minimum price searchValue
        const filtredItems = items.filter((item) => 
            item.title.toLowerCase().includes(searchValue.toLowerCase())
        );
        return ((isLoading  ? [...Array(8)]  : filtredItems).map((item, index) => (
                //Each card from the compiled list
                <Card
                    key={index}
                    onPlus={(obj) => onAddToCart(obj)}
                    loading={isLoading}
                    {...item}
                />
            ))
        )
    }

    return (
        <div className="content p-40">
            <div className="d-flex align-center justify-between mb-40">
                <h1>{searchValue ? `Search by request: "${searchValue}"` : `All sneakers`}</h1>
                <div className="search-block d-flex">
                    <img src="/img/search.svg" alt="Search"/>
                    { searchValue && (
                        //product name search tag reset button
                        <img 
                            onClick={() => setSearchValue('')} 
                            className="clear cu-p" 
                            src="/img/btn-remove.svg" 
                            alt="Clear" 
                        />
                    )}
                    {/*item name search label input box */}
                    <input onChange={onChangeSearchInput} value={searchValue} placeholder="Search..." />
                </div>
            </div>
            {/*The input field for the list of products set based on the specified minimum price searchValue*/}
            <div className="d-flex flex-wrap">          
                {renderItems()}
            </div>
        </div>
    )
}

export default Home;