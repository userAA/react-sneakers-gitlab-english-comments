import axios from "axios";
import React from "react";

import Card from '../components/Card/index'

function Orders(){
    //the list of all products from all orders from server
    const [orders, setOrders] = React.useState([]);
    //the flag of list formation of all products from all orders with server
    const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        (async () => { 
            try 
            {
                //loading of all orders with server
                //const {data} = await axios.get('https://60d62397943aa60017768e77.mockapi.io/orders');
                const {data} = await axios.get('https://react-sneakers-deploy-server.herokuapp.com/orders');
                //highlight the list of all products from all orders with server
                setOrders(data);  
                //the creating a list of all products from all orders with server is finished
                setIsLoading(false);
            } 
            catch (error) 
            {
                alert('Error when requesting orders');
                console.error(error);
            }
        })();
    }, [])

    return (
        <div className="content p-40">
            {/*Headering */}
            <div className="d-flex align-center justify-between mb-40">
                <h1>My orders</h1>
            </div>
            {/*Outputing the list of all products on all orders from server or 
               list from eight empty cells if there are no orders on the server. */}
            <div className="d-flex flex-wrap">  
                {(isLoading ? [...Array(8)] : orders).map((items, index) => {  
                    return (
                        items !== undefined ? items.items === undefined ? 
                        <></> 
                        : 
                        (items.items).map((item, index) => (  
                            <Card key={index} loading={isLoading} {...item}/> 
                        ))
                        : 
                        <></> 
                    )           
                })}
            </div>
        </div>
    )
}

export default Orders;