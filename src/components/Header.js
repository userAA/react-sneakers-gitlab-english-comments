import React from 'react'
import {Link} from 'react-router-dom'

import {useCart} from '../hooks/useCart'

function Header(props) 
{
    //the cost of products in the basket of products is being founded from created hook useCart
    const {totalPrice} = useCart();

    return (
        <header className="d-flex justify-between align-center p-40">
            {/*Header site */}
            <Link to="/">
                <div className="d-flex align-center">
                    <img width={40} height={40} src="/img/logo.png" alt="Logotype"/>
                    <div>
                        <h3 className="text-uppercase">React Sneakers</h3>
                        <p className="opacity-5">Shop for the best sneakers</p>
                    </div>
                </div>
            </Link>
            <ul className="d-flex">
                {/*Appearing of basket with selected product */}
                <li onClick={props.onClickCart} className="mr-30 cu-p">
                    <img width={18} height={18} src="/img/cart.svg" alt="Basket"/>
                    <span>{totalPrice} rub.</span>
                </li>
                {/*Appearing of list of all selected products according to all baskets*/}
                <li>
                    <Link to="/orders">
                        <img width={18} height={18} src="/img/user.svg" alt="User"/>
                    </Link>
                </li>
            </ul>
        </header>
    )
}

export default Header;