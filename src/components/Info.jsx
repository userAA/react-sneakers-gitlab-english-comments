import React from 'react'
import AppContext from '../context'

//information about what happens to the basket of productss either it is empty or the goods from it were sold to someone
const Info = ({title, image, description}) => 
{
    //from context we finding the flag of opening basket of products
    const {setCartOpened} = React.useContext(AppContext); 

    return (
        <div className="cartEmpty d-flex align-center justify-center flex-column flex">
            <img 
                className="mb-20" 
                width="120px" 
                src={image} 
                alt="Empty" 
            />
            <h2>{title}</h2>
            <p className="opacity-6">{description}</p>
            {/*The button of closing information about the transaction with the basket of products and returning in to page shop */}
            <button onClick={() => setCartOpened(false)} className="greenButton">
                <img src="/img/arrow.svg" alt="Arrow" />
                Go back
            </button>
        </div>
    )
}

export default Info;