import React from 'react'
import axios from 'axios'

import Info from '../Info'
import {useCart} from '../../hooks/useCart'

import styles from './Drawer.module.scss';

function Drawer({onClose, onRemove, items=[], opened}) {
    //information about the order from useCart
    const {cartItems, setCartItems, totalPrice} = useCart();
    //ID of the next order
    const [orderId, setOrderId] = React.useState(null);
    //implementation flag of the result
    const [isOrderComplete, setIsOrderComplete] = React.useState(false);
    //the flag of loading an order to the server 
    const [isLoading, setIsLoading] = React.useState(false);

    //fixing order in cart (making an order)
    const onClickOrder = async () => 
    {
        try 
        {
            //the beginning of sending an order from the basket of products to the server
            setIsLoading(true);
            //sending the order in to server, data - what is sent
            //const {data} = await axios.post('https://60d62397943aa60017768e77.mockapi.io/orders', {items: cartItems});
            const {data} = await axios.post('https://react-sneakers-deploy-server.herokuapp.com/orders', {items: cartItems});
            //fixing ID of sended order in to server
            setOrderId(data.id);
            //fixing flag, speaking about, that order in to server is sended
            setIsOrderComplete(true);
            //emptying the basket of productss
            setCartItems([]);
        } 
        catch (error) 
        {
            alert('Не удалось создать заказ.');
        }
        //the process of sending the order from cart of products in to sever is finished
        setIsLoading(false);
    }

    return (
        <div className={`${styles.overlay} ${opened ? styles.overlayVisible : ''}`}>
            <div className={styles.drawer}>
                {/*Heading */}
                <h2 className="d-flex justify-between mb-30">
                    Basket
                    {/*The button of hiding basket */}
                    <img
                        onClick={onClose}
                        className="cu-p" 
                        src="/img/btn-remove.svg" 
                        alt="Close" 
                    />
                </h2>

                {/*List of products in the shopping cart before placing an order */}
                {items.length > 0 ? (
                    <div className="d-flex flex-column flex">
                        <div className="items">
                            {items.map((obj) =>(
                                <div key={obj.id} className="cartItem d-flex align-center mb-20">
                                    {/*Product picture*/}
                                    <div 
                                        style={{ backgroundImage: `url(${obj.imageUrl})` }}
                                        className="cartItemImg"  
                                    >
                                    </div>
                                    <div className="mr-20 flex">
                                        {/*Name product*/}
                                        <p className="mb-5">{obj.title}</p>
                                        {/*Price product*/}
                                        <b>{obj.price} rub.</b>
                                    </div>
                                    {/*The button of removing product from basket*/}
                                    <img
                                        onClick={() => onRemove(obj.id)}
                                        className="removeBtn" 
                                        src="/img/btn-remove.svg" 
                                        alt="Remove" 
                                    />
                                </div>
                            ))}
                        </div>

                        <div className="cartTotalBlock">
                            <ul>
                                {/*The price of total order */}
                                <li>
                                    <span>Total:</span>
                                    <div></div>
                                    <b>{totalPrice} rub.</b>
                                </li>
                                {/*Tax on the price of the entire order*/}
                                <li>
                                    <span>Tax 5%:</span>
                                    <div></div>
                                    <b>{5*totalPrice/100} rub.</b>
                                </li>
                            </ul>
                            {/*The button for placing the entire order*/}
                            <button disabled={isLoading} onClick={onClickOrder} className="greenButton">
                                Place an order
                                <img src="/img/arrow.svg" alt="Arrow"/>
                            </button>
                        </div>
                    </div>
                ) : (
                    //The information of successful placing of total order
                    <Info 
                        title={ isOrderComplete ? "The order has been placed!" : "The basket is empty"} 
                        description={ isOrderComplete ? `Your order #${orderId} it will be delivered by courier soon` :
                         "Add at least one pair of sneakers to make an order."} 
                        image={ isOrderComplete ? "/img/complete-order.jpg" : "/img/empty-cart.jpg"}
                    />
                )}
            </div>
        </div>
    )
}

export default Drawer;