import React from 'react'
import AppContext from '../context';

export const useCart = () => {
    //from context finding cart product
    const {cartItems, setCartItems} = React.useContext(AppContext);
    //we consider the full cost of goods across the entire basket
    const totalPrice = cartItems.reduce((sum, obj) => obj.price + sum, 0);
    //the result of the generated hook
    return {cartItems, setCartItems, totalPrice};
}